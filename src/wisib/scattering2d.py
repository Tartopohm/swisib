#!/usr/bin/env python3

"""Model solving for the scattering of a wave by a 2D array of ice floes.

This models idealizes ice floes as homogeneous and isotropic elastic bodies
floating on a perfect, inviscid fluid with an irrotatinal flow.
Linear invariance is assumed in the horizontal direction normal to the wave
propagation. The wave is monochromatic and non-linear effects are discarded.
"""

from __future__ import annotations


__all__ = [
    "Ice",
    "IceCoupled",
    "Floe",
    "FloeCoupled",
    "Ocean",
    "OceanCoupled",
    "Wave",
    "Domain",
    "FloeOverlapError",
]

import itertools
import functools
import math
from typing import Sequence

import numpy as np
import scipy.sparse as ss
from scipy.optimize import brentq, fminbound, newton
from scipy.signal import find_peaks
from scipy.special import ive


F_EPS = np.finfo(float).eps
PI = np.pi


class Ice:
    """Encapsulate ice mechanical properties.

    Parameters
    ----------
    density : float
        Ice density in kg m**-3.
    poissons_ratio : float
        Ice Poisson's ratio.
    thickness : float
        Ice thickness in m.
    viscosity : float
        Ice viscosity parameter in Pa s m**-1.
    youngs_modulus : float
        Ice Young's modulus in Pa.

    Attributes
    ----------
    density : float
    poissons_ratio : float
    thickness : float
    viscosity : float
    youngs_modulus : float

    """

    def __init__(
        self,
        density: float = 922.5,
        poissons_ratio: float = 0.3,
        thickness: float = 1,
        viscosity: float = 0,
        youngs_modulus: float = 6e9,
    ) -> None:
        """Initialise self."""
        self.__density = density
        self.__poissons_ratio = poissons_ratio
        self.__thickness = thickness
        self.__viscosity = viscosity
        self.__youngs_modulus = youngs_modulus

    def __eq__(self, other: Ice) -> bool:
        """Equals method.

        Two IceProperties are considered equal when their density,
        Poisson's ratio, thickness and Young's modulus match.

        Parameters
        ----------
        other : IceProperties
            other

        Returns
        -------
        bool

        """
        if not isinstance(other, Ice):
            return NotImplemented

        return (
            self.density == other.density
            and self.poissons_ratio == other.poissons_ratio
            and self.thickness == other.thickness
            and self.viscosity == other.viscosity
            and self.youngs_modulus == other.youngs_modulus
        )

    def __hash__(self) -> int:
        """Hash method.

        Hash is based on the attributes defining equality.

        Returns
        -------
        int

        """
        return hash(
            (
                self.density,
                self.poissons_ratio,
                self.viscosity,
                self.thickness,
                self.youngs_modulus,
            )
        )

    @property
    def density(self) -> float:
        """Ice density in kg m**-3."""
        return self.__density

    @property
    def poissons_ratio(self) -> float:
        """Ice Poisson's ratio."""
        return self.__poissons_ratio

    @property
    def thickness(self) -> float:
        """Ice thickness in m."""
        return self.__thickness

    @property
    def viscosity(self) -> float:
        """Ice viscosity parameter in Pa s m**-1."""
        return self.__viscosity

    @property
    def youngs_modulus(self) -> float:
        """Ice Young's modulus in Pa."""
        return self.__youngs_modulus


class IceCoupled(Ice):
    """Extend Ice to hold the parameters determining the scattering.

    The scattering properties of the ice don't depend only on its mechanical
    properties, but on features of the ocean and the forcing wave as well.

    Parameters
    ----------
    ice : Ice
        Set of mechanical properties.
    wave : Wave
        Wave whose period forces the dispersion relation.
    ocean : OceanCoupled
        Fluid interacting with the ice.
    nk : int
        Number of evanescent modes to be computed.
    nu : int
        Number of evanescent modes used to expand the accessory function.
    nv : int
        Number of evanescent modes kept in the scattering matrices.

    Attributes
    ----------
    draft : float
    dud : float
    flex_rigidity : float
    inner_products : 1D array-like of complex
    reflection_i : 2D array-like of complex
    reflection_w : 2D array-like of complex
    transmission_itow : 2D array-like of complex
    transmission_wtoi : 2D array-like of complex
    wavelength : float
    wave_numbers : 1D array-like of complex

    """

    def __init__(
        self, ice: Ice, wave: Wave, ocean: OceanCoupled, nk: int, nu: int, nv: int
    ):
        """Initialise self."""
        super().__init__(
            ice.density,
            ice.poissons_ratio,
            ice.thickness,
            ice.viscosity,
            ice.youngs_modulus,
        )
        draft = self.thickness * self.density / ocean.density
        dud = ocean.depth - draft
        if wave.period < 2 * PI * np.sqrt(draft / wave.gravity):
            print("ω > sqrt(g/d): root finding may behave unpredictably.")
        ll = (self.flex_rigidity / (ocean.density * wave.angular_frequency2)) ** 0.2
        aa = (1 - wave._c_alpha * draft) / (wave._c_alpha * ll)
        if ice.viscosity > 0:
            aa -= 1j * ice.viscosity / (ocean.density * wave.angular_frequency * ll)
        rr = dud / ll
        ss = self._compute_wave_numbers(aa, rr, nk)
        self.__wave_numbers = ss / ll
        self.__inner_products = self._compute_inner_products(aa, ll, rr, ss)
        self.__draft = draft
        self.__dud = dud
        self.__c_length = ll
        self.__c_stiffness = aa
        s_matrices = self._compute_scattering(wave, ocean, nu, nv)
        self.__reflection_i = s_matrices[0]
        self.__reflection_w = s_matrices[1]
        self.__transmission_itow = s_matrices[2]
        self.__transmission_wtoi = s_matrices[3]

    @property
    def draft(self) -> float:
        """Submerged ice thickness in m."""
        return self.__draft

    @property
    def dud(self) -> float:
        """Height of the water column underneath the ice in m."""
        return self.__dud

    @property
    def inner_products(self) -> np.ndarray:
        """Array of inner products of the wave modes with themselves in m.

        An inner product is defined on the space spawned by the wave modes.
        This modes are orthogonal with respect to that product, hence there is
        as many non-null products as there is modes.
        """
        return self.__inner_products

    @property
    def reflection_i(self) -> np.ndarray:
        """Within-ice reflection matrix."""
        return self.__reflection_i

    @property
    def reflection_w(self) -> np.ndarray:
        """Within-water reflection matrix."""
        return self.__reflection_w

    @property
    def transmission_itow(self) -> np.ndarray:
        """Ice-to-water transmission matrix."""
        return self.__transmission_itow

    @property
    def transmission_wtoi(self) -> np.ndarray:
        """Water-to-ice transmission matrix."""
        return self.__transmission_wtoi

    @property
    def wave_numbers(self) -> np.ndarray:
        """Vector of wave numbers in rad m**-1.

        Index 0 has positive real and imag parts.
        Index 1 has negative real and positive imagniary parts.
        Index 2 corresponds to the main propagating mode.
        Index 3 onwards correspond to the evanescent modes.
        """
        return self.__wave_numbers

    @property
    def _c_length(self):
        """Characteristic vertical length of the ice."""
        return self.__c_length

    @property
    def _c_stiffness(self) -> float:
        """Adimentionalise stiffness of the ice."""
        return self.__c_stiffness

    @functools.cached_property
    def flex_rigidity(self) -> float:
        """Ice flexural rigidity in Pa m**3.

        Returns
        _______
        flex_rigidity : float
        """
        return (
            self.youngs_modulus
            * self.thickness**3
            / (12 * (1 - self.poissons_ratio**2))
        )

    @functools.cached_property
    def wavelength(self) -> float:
        """Wavelength in m.

        It corresponds to the real part of the main propagating mode.
        """
        return 2 * PI / np.real(self.wave_numbers[2])

    def _compute_inner_products(self, aa, ll, rr, ss):
        """Compute the inner product associated with each mode.

        The inner product is defined in a way that different modes are
        orthogonal. Hence, only the inner product of a mode with itself
        is relevant.

        Parameters
        ----------
        aa : float
            Coefficient of the 1st degree term.
        ll : float
            Caracteristic length.
        rr : float
            Scaled water depth.
        ss : 1D array-like of complex
            Scaled wave numbers.

        Returns
        -------
        1D array-like of float
            The computed inner products.

        """
        return ll * (rr + (5 * ss**4 + aa - rr) / ((ss * (ss**4 + aa)) ** 2)) / 2

    def _compute_scattering(
        self, wave: Wave, ocean: OceanCoupled, nu: int, nv: int
    ) -> tuple[np.ndarray]:
        """Determine the scattering caused by an ice edge.

        This method computes the reflection and transmission matrices
        describing the interaction `wave`, `ocean` and `self`.

        Parameters
        ----------
        wave : Wave
            Wave being scattered.
        ocean : OceanCoupled
            Ocean at the surface of which travels the wave.
        nu : int
            Number of evanescent modes used to expand the accessory function.
        nv : int
            Number of evanescent modes kept to compute a solution.

        Returns
        -------
        tuple of 2D array_like of complex
            The reflection and transmission matrices
            (ice to ice,
             water to water,
             ice to water,
             water to ice).

        """
        BETA = 1 / 6
        OP_FACTOR = self.dud * math.gamma(BETA) * 2**BETA

        iwn_dud = self.wave_numbers * self.dud
        own_dud = ocean.wave_numbers * self.dud
        own_dpt = ocean.wave_numbers * ocean.depth

        bessel_order = 2 * np.arange(0, nu + 1) + BETA
        bessel_arg = own_dud
        sc = np.cos(np.imag(own_dpt))
        k0 = np.real(ocean.wave_numbers[0])
        sc[0] = (1 + np.exp(-2 * ocean.depth * k0)) * np.exp(k0 * self.draft) / 2
        m_op_water = (
            (bessel_arg**-BETA / sc)[:, None]
            * ive(*np.meshgrid(bessel_order, bessel_arg))
            * bessel_order
        )
        m_op_water *= OP_FACTOR / 2

        bessel_arg = iwn_dud
        a, b = np.real(iwn_dud), np.imag(iwn_dud)
        a[1] *= -1
        b[1] *= -1
        sc = np.exp(1j * b) + np.exp(-(2 * a + 1j * b))
        # sc[1] = np.exp(-1j*b[1]) + np.exp(2*a[1] + 1j*b[1])
        m_op_ice = (
            (bessel_arg**-BETA / sc)[:, None]
            * ive(*np.meshgrid(bessel_order, bessel_arg))
            * bessel_order
        )
        m_op_ice *= OP_FACTOR

        m_ip_water = 1 / (ocean.wave_numbers * ocean.inner_products)
        m_ip_ice = 1 / (self.wave_numbers * self.inner_products)

        kinv = np.linalg.inv(
            (m_op_water.T * m_ip_water) @ m_op_water
            + (m_op_ice.T * m_ip_ice) @ m_op_ice
        )

        nvp1, nvp3, nvp4 = nv + 1, nv + 3, nv + 4
        m_ip_water = m_ip_water[:nvp1]
        m_ip_ice = m_ip_ice[:nvp3]
        m_op_water = m_op_water[:nvp1, :]
        m_op_ice = m_op_ice[:nvp3, :]

        matmpp = (m_ip_ice[:, None] * m_op_ice) @ kinv @ m_op_ice.T
        matmpm = (m_ip_ice[:, None] * m_op_ice) @ kinv @ m_op_water.T
        matmmm = (m_ip_water[:, None] * m_op_water) @ kinv @ m_op_water.T
        matmmp = (m_ip_water[:, None] * m_op_water) @ kinv @ m_op_ice.T

        mat_t = self.wave_numbers[: nv + 3] ** 3 * np.tanh(
            self.wave_numbers[: nv + 3] * self.dud
        )
        mat_f = (
            self.flex_rigidity
            * self.wave_numbers[: nv + 3] ** 2
            * np.tanh(self.wave_numbers[: nv + 3] * self.dud)
            / (
                1j
                * self.inner_products[: nv + 3]
                * ocean.density
                * wave.angular_frequency2
            )
        )
        satmpp = -matmpp.copy()
        d = satmpp.ravel()[::nvp4]
        d += 1  # equivalent to satmpp += np.eye(satmpp.shape)

        denom = mat_t @ satmpp @ mat_f
        mat_ft = mat_f[:, None] @ mat_t[None]

        mat_rlp = -2 * (matmmm + (matmmp @ mat_ft @ matmpm) / denom)
        d = mat_rlp.ravel()[:: nv + 2]
        d += 1  # idem
        mat_rlm = -2 * (matmpp + (satmpp @ mat_ft @ satmpp) / denom)
        d = mat_rlm.ravel()[::nvp4]
        d += 1  # idem
        mat_tlp = 2 * (matmpm - (satmpp @ mat_ft @ matmpm) / denom)
        mat_tlm = 2 * (matmmp - (matmmp @ mat_ft @ satmpp) / denom)

        transmission_wtoi = mat_tlp
        transmission_itow = mat_tlm
        reflection_w = mat_rlp
        reflection_i = mat_rlm

        return (reflection_i, reflection_w, transmission_itow, transmission_wtoi)

    def _compute_wave_numbers(self, aa, rr, nk):
        """Determine `nk+3` wave numbers.

        This methods solve for s the dispersion relation
        (s**5 + a*s)*tanh(r*s) = 1.

        Parameters
        ----------
        aa : float
            Coefficient of the 1st degree term.
        ll : float
            Caracteristic length.
        rr : float
            Scaled water depth.
        nk : int
            Number of evanescent modes to be computed.

        Returns
        -------
        1D array-like of complex
            The computed wave-numbers.

        """
        roots = np.empty(nk + 3, complex)

        def dr(ss: complex) -> complex:
            # Dispersion relation (form f(s) = 0), for an elastic plate,
            # admitting one positive real root when viscosity is set to 0.
            return (ss**4 + aa) * ss * np.tanh(rr * ss) - 1

        def dr_i(ss: float) -> float:
            # Dispersion relation (form f(s) = 0), for an elastic plate,
            # admitting an infinity of positive real roots when viscosity
            # is set to 0.
            return (ss**4 + aa) * ss * np.sin(rr * ss) + np.cos(rr * ss)

        # NR's and H's methods use f/f' and f''/f'.
        # f, f' and f'' can hence be scalled before being passed to
        # scipy's function. Following expressions for dr_d and dr_dd
        # take advantage of the fact that deriving cosh and sinh is
        # more straightforward than deriving tanh.
        def dr_d(ss: complex) -> complex:
            # Derivative of dr(ss)*cosh(rr*ss) with respect to ss,
            # scaled by cosh(rr*ss).
            return (5 * ss**4 + aa - rr) * np.tanh(rr * ss) + rr * ss * (ss**4 + aa)

        def dr_dd(ss: complex) -> complex:
            # Second derivative of dr(ss)*cosh(rr*ss) with respect to ss,
            # scaled by cosh(rr*ss).
            ss4 = ss**4
            return (20 * ss**3 + rr**2 * (ss4 + aa)) * ss * np.tanh(
                rr * ss
            ) + rr * (2 * (5 * ss4 + aa) - rr)

        # The first guess is given by the deep water approximation.
        roots_dw = np.roots([1, 0, 0, 0, aa, -1])

        # If the plate modelling the ice is fully elastic, dr_i has only
        # real roots. Brent's method is used to enforce that the
        # solutions belong to the correct intervals.
        # Otherwise, use of the Halley's method and additional checks.
        if np.imag(aa) == 0:
            guess = np.real(roots_dw[np.isclose(np.imag(roots_dw), 0)])
            roots[2] = newton(dr, fprime=dr_d, x0=guess, tol=1e-14)
            assert np.real(roots[2]) > 0 and np.imag(roots[2]) == 0
            guess = roots_dw[
                np.logical_and(np.real(roots_dw) > 0, np.imag(roots_dw) > 0)
            ]
            roots[0] = newton(dr, fprime=dr_d, x0=guess, tol=1e-14)
            assert np.real(roots[0]) > 0 and np.imag(roots[0]) > 0
            roots[1] = -np.conj(roots[0])
            if nk > 0:
                sPI = PI / rr
                roots[3] = 1j * brentq(dr_i, 0, sPI, xtol=1e-16)
                roots[4:] = [
                    1j * brentq(dr_i, (m + 0.5) * sPI, (m + 1) * sPI, xtol=1e-16)
                    for m in range(1, nk)
                ]
                assert (np.imag(roots[3:]) > 0).all() and (
                    np.real(roots[3:]) == 0
                ).all()
        else:

            def dr_i_d(ss: complex) -> complex:
                # Derivative of dr_i with respect to ss.
                return (5 * ss**4 + aa - rr) * np.sin(rr * ss) + rr * (
                    ss**4 + aa
                ) * ss * np.cos(rr * ss)

            def dr_i_dd(ss: complex) -> complex:
                # Second derivative of dr_i with respect to ss.
                return (20 * ss**3 - rr**2 * (ss**4 + aa) * ss) * np.sin(
                    rr * ss
                ) + rr * (10 * ss**4 + 2 * aa - rr) * np.cos(rr * ss)

            guess = roots_dw[np.abs(np.imag(roots_dw)).argmin()]  # semi-real
            roots[2] = newton(dr, fprime=dr_d, fprime2=None, x0=guess, tol=1e-14)
            assert np.real(roots[2]) > 0 and np.imag(roots[2]) > 0

            # complex, 1st quadrant
            guess = roots_dw[
                np.logical_and(np.real(roots_dw) > 0, np.imag(roots_dw) > 0)
            ]
            if type(guess) is np.ndarray:
                guess = guess[np.imag(guess).argmax()]
            roots[0] = newton(dr, fprime=dr_d, fprime2=None, x0=guess, tol=1e-12)
            assert np.real(roots[0]) > 0 and np.imag(roots[0]) > 0

            # complex, 2nd quadrant
            # initial guess taken as opposite of the 4th quadrant's root
            guess = roots_dw[
                np.logical_and(np.real(roots_dw) > 0, np.imag(roots_dw) < 0)
            ]
            if type(guess) is np.ndarray:
                guess = guess[np.imag(guess).argmax()]
            roots[1] = newton(
                dr, fprime=dr_d, fprime2=dr_dd, x0=-guess, tol=1e-12, maxiter=100
            )
            assert np.real(roots[1]) < 0 and np.imag(roots[1]) > 0

            if nk > 0:
                sPI = PI / rr
                # bornes from Tim Williams' thesis; valid if aa >=0 ...
                roots[3] = 1j * newton(
                    dr_i, fprime=dr_i_d, fprime2=dr_i_dd, x0=sPI / 2, tol=1e-14
                )
                assert 0 < np.imag(roots[3]) < sPI
                roots[4:] = [
                    1j * newton(dr_i, fprime=dr_i_d, x0=(j + 0.75) * sPI, tol=1e-14)
                    for j in range(1, nk)
                ]
                interval_chck = np.arange(2, nk + 1) * sPI
                assert np.logical_and(
                    interval_chck - 0.5 < np.imag(roots[4:]),
                    np.imag(roots[4:]) < interval_chck,
                ).all()

        return roots


@functools.total_ordering
class Floe:
    """Locates an ice floe in space.

    Parameters
    ----------
    left_edge : float
        Location of the left edge on the x-axis in m.
    length : float
        Length of the floe in m.
    ice : Ice
        Mechanical properties of the floe.

    Attributes
    ----------
    ice : Ice
    left_edge : float
    length : float

    """

    __slots__ = ("__left_edge", "__length", "__ice")

    def __init__(
        self, left_edge: float = 0, length: float = 100, ice: Ice = Ice()
    ) -> None:
        """Initialise self."""
        self.__ice = ice
        self.__left_edge = left_edge
        self.__length = length

    def __eq__(self, other: Floe) -> bool:
        """Equal method.

        Two floes are deemed equal when their left edges are the same.

        Parameters
        ----------
        other : Floe
            other

        Returns
        -------
        bool

        """
        return self.left_edge == other.left_edge

    def __lt__(self, other: Floe) -> bool:
        """Lower than method.

        Compares the coordinate (left edge) of two floes.

        Parameters
        ----------
        other : Floe
            other

        Returns
        -------
        bool

        """
        return self.left_edge < other.left_edge

    @property
    def ice(self) -> Ice:
        """Set of properties determining the scattering."""
        return self.__ice

    @property
    def left_edge(self) -> float:
        """Location of the left edge on the x-axis in m."""
        return self.__left_edge

    @property
    def length(self) -> float:
        """Length of the floe in m."""
        return self.__length

    @functools.cached_property
    def right_edge(self) -> float:
        """Location of the right edge on the x-axis in m."""
        return self.left_edge + self.length

    # @property
    # def phase(self) -> np.ndarray:
    #     nv = self.reflection_neg.shape[0]
    #     return self.wave_numbers[:nv]*self.length


class FloeCoupled(Floe):
    """Extend `Floe` to expose fields radiated by the floe.

    Parameters
    ----------
    floe : Floe
        Spatial position of an ice floe.
    r_fields : tuple of 1D array-like of complex
        Wave fields radiated by the ice floe.

    Attributes
    ----------
    amplitude_iw_neg : 1D array-like of complex
    amplitude_iw_pos : 1D array-like of complex
    amplitude_ww_neg : 1D array-like of complex
    amplitude_ww_pos : 1D array-like of complex

    """

    __slots__ = (
        "__amplitude_iw_neg",
        "__amplitude_iw_pos",
        "__amplitude_ww_neg",
        "__amplitude_ww_pos",
    )

    def __init__(self, floe: Floe, r_fields: tuple[np.ndarray]):
        """Initialise self."""
        super().__init__(floe.left_edge, floe.length, floe.ice)
        self.__amplitude_iw_neg = r_fields[0]
        self.__amplitude_iw_pos = r_fields[1]
        self.__amplitude_ww_neg = r_fields[2]
        self.__amplitude_ww_pos = r_fields[3]

    @property
    def amplitude_iw_neg(self) -> np.ndarray:
        """Array of  wave amplitudes in m**2 s**-1.

        Right edge-radiated, negative-propagating.
        """
        return self.__amplitude_iw_neg

    @property
    def amplitude_iw_pos(self) -> np.ndarray:
        """Vector of  wave amplitudes in m**2 s**-1.

        Left edge-radiated, positive-propagating.
        """
        return self.__amplitude_iw_pos

    @property
    def amplitude_ww_neg(self) -> np.ndarray:
        """Vector of  wave amplitudes in m**2 s**-1.

        Left edge-radiated, negative-propagating.
        """
        return self.__amplitude_ww_neg

    @property
    def amplitude_ww_pos(self) -> np.ndarray:
        """Vector of  wave amplitudes in m**2 s**-1.

        Right edge-radiated, positive-propagating.
        """
        return self.__amplitude_ww_pos

    def strain(self, x: np.ndarray, wave: Wave) -> np.ndarray:
        """Compute the strain undergone by the Floe.

        The deplacement of the (ice-covered) surface due to the propagating
        wave warps the floe. The elastic strain is proportional to the
        curvature of the surface (2nd derivative with respect to space in the
        horizontal direction). Invariance of the strain field along the
        vertical direction is assumed (thin beam geometry).

        Parameters
        ----------
        x : 1D array-like of float
            Array on which to sample the strain. Should be enclosed in the
            floe's boundaries.
        wave : Wave
            Wave whose period is associated with the strain.

        Returns
        -------
        np.ndarray
            Strain values corresponding to the elements in `x`.

        """
        length = self.length
        left_edge = self.left_edge
        dud = self.ice.dud
        wave_numbers = self.ice.wave_numbers
        # -i omega eta = \pd{phi, z}
        # => strain = i h \pd{phi, [xxz]} / (2*om)
        # factor -1 factored on the return line
        if length == np.inf:
            s = sum(
                [
                    wp_n
                    * np.exp(1j * k_n * (x - left_edge))
                    * k_n**3
                    * np.tanh(k_n * dud)
                    for (wp_n, k_n) in zip(self.amplitude_iw_pos, wave_numbers)
                ]
            )
        else:
            s = sum(
                [
                    (
                        wp_n * np.exp(1j * k_n * (x - left_edge))
                        + wn_n * np.exp(-1j * k_n * (x - (left_edge + length)))
                    )
                    * k_n**3
                    * np.tanh(k_n * dud)
                    for (wp_n, wn_n, k_n) in zip(
                        self.amplitude_iw_pos, self.amplitude_iw_neg, wave_numbers
                    )
                ]
            )

        return -1j * self.ice.thickness * s / (2 * wave.angular_frequency)

    def strain_extrema(
        self, wave: Wave, n_coord: int = 1, n_samples: int = 16, clipping_th: int = 10
    ) -> np.ndarray:
        """Return the coordinates of strain extrema.

        This method proceeds in two passes. The floe is first sampled between
        its first edge and the limit set by `clipping_th`, with a
        spacial frequency corresponding to `n_samples`. The strain is computed
        for these values, and the local extrema of the real part detected.
        These serve as entry data for determining the actual extrema with an
        optimization method.
        The method relies on the observation that the strain is
        quasi-periodic in space, with a period corresponding to the wave's
        main propagating mode.
        The returned coordinates are sorted by decreasing absolute real strain.

        Parameters
        ----------
        wave : Wave
            Wave whose period is associated with the strain.
        n_coord : int
            Number of coordinates to return. If None, all the determined
            ones are returned. Must be strictly positive.
        n_samples : int
            Number of points used to sample the length of the floe, per
            wavelength.
        clipping_th : int
            Number of wavelengths to consider into the floe. If None and the
            floe is finite, the whole Floe is sampled. If None and the floe
            is infinite, internally set to 2 to ensure capturing the absolute
            extrema.

        Returns
        -------
        1D array-like of float
            Coordinates of the strain extrema.

        """
        wl_i = self.ice.wavelength
        length = self.length
        left_edge = self.left_edge

        if clipping_th is not None:
            r_offset = min(length, clipping_th * wl_i)
            n_elem = int(np.ceil(r_offset / wl_i)) * n_samples + 1
        else:
            if length == np.inf:
                CT = 2
                r_offset = CT * wl_i
                n_elem = CT * n_samples + 1
            else:
                # rounded up number of wl in the floe length
                r_offset = length
                n_elem = int(np.ceil(r_offset / wl_i)) * n_samples + 1
        x_sampling = np.linspace(left_edge, left_edge + r_offset, n_elem)

        temp_strain = self.strain(x_sampling, wave)
        # Get an approximation of the extrema locations;
        # find_peaks returns the indices as the 1st member of a tuple
        pks_idx = find_peaks(np.abs(np.real(temp_strain)))[0]
        # Get the mid-points of every interval defined by the approximated
        # extrema (and x_sampling boundaries) to serve as bracketing intervals
        # for Brent's method.
        msk = np.zeros(pks_idx.shape[0] + 1, dtype=int)
        msk[1:-1] = np.round((pks_idx[1:] + pks_idx[:-1]) / 2).astype(int)
        msk[-1] = x_sampling.shape[0] - 1
        pks_bounds = x_sampling[msk]

        def dstrain(x: np.ndarray, floe: FloeCoupled, wave: Wave) -> np.ndarray:
            # Compute the derivative of the strain (Floe.strain) with
            # respect to x.
            length = floe.length
            left_edge = floe.left_edge
            dud = floe.ice.dud
            wave_numbers = floe.ice.wave_numbers
            if length == np.inf:
                ds = sum(
                    [
                        wp_n
                        * np.exp(1j * k_n * (x - left_edge))
                        * k_n**4
                        * np.tanh(k_n * dud)
                        for (wp_n, k_n) in zip(floe.amplitude_iw_pos, wave_numbers)
                    ]
                )
            else:
                ds = sum(
                    [
                        (
                            wp_n * np.exp(1j * k_n * (x - left_edge))
                            - wn_n * np.exp(-1j * k_n * (x - (left_edge + length)))
                        )
                        * k_n**4
                        * np.tanh(k_n * dud)
                        for (wp_n, wn_n, k_n) in zip(
                            floe.amplitude_iw_pos, floe.amplitude_iw_neg, wave_numbers
                        )
                    ]
                )

            # Return the abs value as we're looking for a zero, not
            # a negative minimum
            return np.abs(
                floe.ice.thickness * np.real(ds) / (2 * wave.angular_frequency)
            )

        # Get the extrema locations with a better accuracy
        x_extrema = np.array(
            [
                fminbound(dstrain, inf, sup, args=(self, wave))
                for inf, sup in zip(pks_bounds[:-1], pks_bounds[1:])
            ]
        )
        n_c = (
            len(x_extrema) if n_coord is None or n_coord >= len(x_extrema) else n_coord
        )
        return x_extrema[
            (-np.abs(np.real(self.strain(x_extrema, wave)))).argsort()[:n_c]
        ]


class Ocean:
    """Represents the fluid bearing the floes.

    Encapsulates the properties of an incompressible ocean of finite,
    constant depth and given density.

    Parameters
    ----------
    depth : float
        Ocean constant and finite depth in m.
    density : float
        Ocean constant density in kg m**-3.

    Attributes
    ----------
    density : float
    depth : float

    """

    def __init__(self, depth: float = 1000, density: float = 1025) -> None:
        """Initialise self."""
        self.__depth = depth
        self.__density = density

    @property
    def density(self) -> float:
        """Ocean density in kg m**-3."""
        return self.__density

    @property
    def depth(self) -> float:
        """Ocean depth in m."""
        return self.__depth


class OceanCoupled(Ocean):
    """Extend `Ocean` to include wave-dependent quantities.

    Parameters
    ----------
    ocean : Ocean
        `Ocean` instance to extend.
    inner_products : 1D array-like of float
        Inner products of the vertical modes in m.
    wave_numbers : 1D array-like of complex
        Wave numbers in rad m**-1.

    Attributes
    ----------
    inner_products : 1D array-like of complex
    wavelength : float
    wave_numbers : 1D array-like of complex

    """

    def __init__(self, ocean: Ocean, wave: Wave, nk: int) -> None:
        """Initialise self."""
        super().__init__(ocean.depth, ocean.density)
        alpha = wave._c_alpha
        self.__wave_numbers = self.compute_wave_numbers(alpha, nk)
        self.__inner_products = self.compute_inner_products(alpha)

    @property
    def inner_products(self) -> np.ndarray:
        """Array of inner products of the vertical modes with themselves in m.

        An inner product is defined on the space spawned by the wave modes.
        This modes are orthogonal, with respect to that product, hence there is
        as many non-null products as there is modes.
        """
        return self.__inner_products

    @property
    def wave_numbers(self) -> np.ndarray:
        """Array of wave numbers in rad m**-1.

        Index 0 is real.
        Index 1 onwards are imaginary.
        """
        return self.__wave_numbers

    @functools.cached_property
    def wavelength(self) -> float:
        """Wavelength in m.

        It corresponds to the only real wave number.
        """
        return 2 * PI / np.real(self.wave_numbers[0])

    def compute_inner_products(self, alpha: float) -> np.ndarray:
        """Compute the inner product associated with each mode.

        Parameters
        ----------
        alpha : float
            Deep water positive, real wave number.
        wave_numbers : 1D array-like of complex
            The actual wave numbers.

        Returns
        -------
        1D array-like of float
            The computed inner products.

        """
        ip = (self.depth * (self.wave_numbers**2 - alpha**2) + alpha) / (
            2 * self.wave_numbers**2
        )
        assert np.allclose(np.imag(ip), 0)
        return np.real(ip)

    def compute_wave_numbers(self, alpha: float, nk: int) -> np.ndarray:
        """Determine nk+1 wave numbers.

        Parameters
        ----------
        alpha : float
            Deep water positive, real wave number.
        nk : int
            Number of evanescent modes to compute.

        Returns
        -------
        1D array-like of complex
            The computed wave-numbers.

        """
        alpha *= self.depth  # rescaling
        roots = np.empty(nk + 1, complex)

        def dr(kk: float) -> float:
            # Dispersion relation (form f(k) = 0), for a free surface,
            # admitting one positive real root.
            return kk * np.tanh(kk) - alpha

        def dr_i(kk: float) -> float:
            # Dispersion relation (form f(k) = 0) for a free surface,
            # admitting an infinity of positive real roots.
            return kk * np.sin(kk) + alpha * np.cos(kk)

        def dr_d(kk: float) -> float:
            # Derivative of dr with respect to kk.
            tt = np.tanh(kk)
            return tt + kk * (1 - tt**2)

        roots[0] = newton(dr, fprime=dr_d, x0=alpha, tol=1e-14)
        assert roots[0] > 0
        roots[1:] = [
            1j * brentq(dr_i, m * PI, (m + 1) * PI, xtol=1e-14) for m in range(nk)
        ]
        return roots / self.depth


class Wave:
    """Represents a monochromatic wave.

    Encapsulates the parameters describing the far-field wave exciting
    the floes array.

    Parameters
    ----------
    amplitude : complex
        Wave amplitude in m (modulus) and phase at the first ice edge
        in rad (argument).
    period : float
        Wave period in m.
    gravity : float
        Acceleration due to gravity in m s**-2.

    Attributes
    ----------
    amplitude : complex
    angular_frequency : float
    angular_frequency2 : float
    gravity : float
    period : float
    potential_amplitude : complex

    """

    def __init__(
        self, amplitude: complex = 0.1, period: float = 8, gravity: float = 9.8
    ) -> None:
        """Initialise self."""
        self.__amplitude = amplitude
        self.__gravity = gravity
        self.__period = period

    @property
    def amplitude(self) -> complex:
        """Wave amplitude in m."""
        return self.__amplitude

    @property
    def gravity(self) -> float:
        """Gravity acceleration in m s**-2."""
        return self.__gravity

    @property
    def period(self) -> float:
        """Wave period in s."""
        return self.__period

    @functools.cached_property
    def angular_frequency(self) -> float:
        """Wave angular frequency in rad s**-1."""
        return 2 * PI / self.period

    @functools.cached_property
    def angular_frequency2(self) -> float:
        """Squared wave angular frequency, for convenience."""
        return self.angular_frequency**2

    @functools.cached_property
    def potential_amplitude(self) -> complex:
        """Wave potential amplitude.

        Amplitude associated to the wave in the context of a potential flow
        in m**2 s**-1.
        """
        return -1j * self.amplitude * self.gravity / self.angular_frequency

    @functools.cached_property
    def _c_alpha(self) -> float:
        return self.angular_frequency2 / self.gravity


class Domain:
    """Link between an Ocean, a Wave, and an array of Floes.

    This class gives access to the functionalities of the model.
    It holds together the geophysical objects so they can communicate.
    It updates each object with the others properties, is responsible for
    determining the scattering due to the ice cover and triggering break up
    events.

    Parameters
    ----------
    wave : Wave
        Monochromatic wave forcing the system.
    ocean : Ocean
        Properties of the fluid region of the domain.
    nk : int
        Number of modes used to compute the scattering kernel.
        nk+1 modes are used for the ocean, and nk+3 for the ice floes.
    nu : int
        Number of modes used to discretize the auxiliary function
        of the Galerkin method (nu+1).
    nv : int
        Number of modes for which a solution (wave amplitude) is computed.
        nv+1 for the ocean, nv+3 for the ice floes.

    Attributes
    ----------
    floes : list of Floe
    ices : set of IceProperties
    nk : int
    nu : int
    nv : int
    ocean : Ocean
    wave : Wave

    """

    def __init__(
        self, wave: Wave, ocean: Ocean, nk: int = 100, nu: int = 20, nv: int = 2
    ) -> None:
        """Initialise self."""
        self.__nk = nk
        self.__nu = nu
        self.__nv = nv
        self.__floes = []
        self.__ocean = OceanCoupled(ocean, wave, nk)
        self.__ices = set()
        self.__wave = wave

    @property
    def floes(self) -> list[FloeCoupled]:
        """Sorted list holding the floes added to the domain."""
        return self.__floes

    @property
    def ices(self) -> set[IceCoupled]:
        """Set holding the different ice properties present in the domain."""
        return self.__ices

    @property
    def nk(self) -> int:
        """Nummber of modes used to compute the scattering kernel.

        nk+1 free surface modes are used.
        nk+3 covered surface modes are used.
        """
        return self.__nk

    @property
    def nu(self) -> int:
        """Nummber of modes used for the Galerkin method.

        nu+1 modes are used to discretise the auxliary function.
        """
        return self.__nu

    @property
    def nv(self) -> int:
        """Nummber of modes for which solutions are computed.

        nv+1 free surface wave potential amplitudes are computed.
        nv+3 covered surface wave potential amplitudes.
        """
        return self.__nv

    @property
    def ocean(self) -> OceanCoupled:
        """Ocean associated with the domain."""
        return self.__ocean

    @property
    def wave(self) -> Wave:
        """Wave associated with the domain."""
        return self.__wave

    def _couple_floe(self, floe: Floe) -> Floe:
        """Couple a new `Floe` to `self`.

        This method check for the existence of an `IceCoupled` instance
        corresponding to `floe.ice` in `self.ices`. If it exists, it is
        used to build the returned `Floe`. Else, it is first determined,
        then used.

        Parameters
        ----------
        floe : Floe
            The floe to be added to the domain.

        Returns
        -------
        Floe
            The floe with ice properties coupled to the domain.

        """
        if floe.ice in self.ices:
            # loop is there only to get the value
            for ice in self.ices:
                if ice is floe.ice:
                    break
        else:
            ice = IceCoupled(floe.ice, self.wave, self.ocean, self.nk, self.nu, self.nv)
            self.ices.add(ice)

        return Floe(floe.left_edge, floe.length, ice)

    def add_floes(self, floes: Sequence[floes]) -> None:
        """Add a sequence of floes to the domain.

        The sequence is sorted and concatenated with the existing floes in
        `self`. The resulting array is sorted again, the absence of
        overlapping floes is verified, then the new floes are coupled
        to the domain and finally the scattering due to the array is
        determined. The resulting sequence is used to update `self.floes`.

        Parameters
        ----------
        floes : sequence of Floe
            The floes to be added.

        Returns
        -------
        None

        Raises
        ------
        FloeOverlapError

        """
        existing_floes = self.floes  # [f.floe for f in self.floes]
        new_floes = sorted(floes)
        lengths = np.array(
            [f.length for f in new_floes] + [f.length for f in existing_floes]
        )
        left_edges = np.array(
            [f.left_edge for f in new_floes] + [f.left_edge for f in existing_floes]
        )
        sort_key = left_edges.argsort()
        left_edges = left_edges[sort_key]
        lengths = lengths[sort_key]
        right_edges = left_edges + lengths
        comp = right_edges[:-1] > left_edges[1:]
        if comp.any():
            idx = comp.argmax()
            raise FloeOverlapError(
                (left_edges[idx], left_edges[idx + 1]),
                (right_edges[idx], right_edges[idx + 1]),
            )

        coupled = [self._couple_floe(f) for f in new_floes]
        floes = sorted(coupled + existing_floes)
        self.__floes = self.scattering(floes)

    def scattering(self, floes: Sequence[Floe]) -> list[FloeCoupled]:
        """Compute the scattering due to the array of floe.

        Each floe in the domain impacts the propagation of the far-field
        wave. Every edge causes some reflection and transmission.
        This method compute the back-propagating and forward-propagating
        waves generated by these edges for a given sequence of `Floe`
        and returns the corresponding sequence of `FloeCoupled`.

        Paramerers
        ----------
        floes : Sequence of Floe
            Array of floes causing the scattering.

        Returns
        -------
        list of FloeCoupled
            The floes in the domain with fully-determined radiated fields.

        """
        if len(floes) >= 1:

            def water_phase(j: int) -> np.ndarray:
                assert 1 <= j <= nf - 1
                return np.exp(
                    1j
                    * self.ocean.wave_numbers[:nvp1]
                    * (
                        floes[j].left_edge
                        - (floes[j - 1].left_edge + floes[j - 1].length)
                    )
                )

            def ice_phase(j: int) -> np.ndarray:
                assert 0 <= j <= nf - 1
                floe = floes[j]
                if not np.isinf(floe.length):
                    return np.exp(1j * floe.length * floe.ice.wave_numbers[:nvp3])
                else:
                    # si la longueur du floe est infinie, toutes les
                    # atténuations associées à des nombres d'ondes avec une
                    # partie imaginaire non-nulle tendent vers 0
                    msk = np.imag(floe.ice.wave_numbers[:nvp3]) == 0
                    phase = np.real(floe.ice.wave_numbers[:nvp3]) % (2 * PI)
                    return np.exp(1j * phase) * msk

            nf = len(floes)
            bs = 4 * self.nv + 8  # size of a square block in lhs matrix
            nvp3 = self.nv + 3
            nvp1 = self.nv + 1
            hbs = bs // 2
            nfm1 = nf - 1
            nfp1 = nf + 1

            # Cinq étapes:
            # départ avec une matrice identité;
            # remplissage de la diagonale-bloc principale, en haut puis
            # en bas, le padding à droite (res à gauche) étant ajouté
            # avec un produit matriciel;
            # construction des 2ndes diagonales-bloc supérieure puis
            # inférieure, le padding étant également ajouté via un
            # produit matriciel.

            # True diagonal
            lhs = ss.eye(nf * bs, format="csr")
            # principal block-diagonal, above the diagonal
            arr = np.empty((nf, hbs, nvp3), complex)
            for i in range(nf):
                ice = floes[i].ice
                arr[i, :nvp1, :] = -ice.transmission_itow * ice_phase(i)
                arr[i, nvp1:, :] = -ice.reflection_i * ice_phase(i)
            indices = np.arange(0, nf)
            indptr = np.zeros(2 * nf + 1)
            indptr[:-1:2] = np.arange(0, nf)
            indptr[1::2] = np.arange(1, nfp1)
            indptr[-1] = nf
            indices1 = np.arange(1, 2 * nf, 2)
            indptr1 = np.arange(0, nfp1)
            diag = ss.bsr_matrix(
                (
                    np.broadcast_to(np.eye(nvp3, hbs, dtype="int8"), (nf, nvp3, hbs)),
                    indices1,
                    indptr1,
                ),
                shape=(nvp3 * nf, bs * nf),
            ).tocsr()
            diag.eliminate_zeros()
            lhs += (
                ss.bsr_matrix(
                    (arr, indices, indptr), shape=(nf * bs, nf * nvp3)
                ).tocsr()
                @ diag
            )
            # principal block-diagonal, below the diagonal
            for i in range(nf):
                ice = floes[i].ice
                arr[i, :nvp3, :] = -ice.reflection_i * ice_phase(i)
                arr[i, nvp3:, :] = -ice.transmission_itow * ice_phase(i)
            indptr[1:] = indptr[0:-1]
            indptr[0] = 0
            diag = ss.bsr_matrix(
                (
                    np.broadcast_to(
                        np.eye(nvp3, hbs, nvp1, dtype="int8"), (nf, nvp3, hbs)
                    ),
                    indices1 - 1,
                    indptr1,
                ),
                shape=(nvp3 * nf, bs * nf),
            ).tocsr()
            diag.eliminate_zeros()
            lhs += (
                ss.bsr_matrix(
                    (arr, indices, indptr), shape=(nf * bs, nf * nvp3)
                ).tocsr()
                @ diag
            )
            # 2nd block-diagonal, upper
            arr = np.empty((nfm1, hbs, nvp1), complex)
            for i in range(nfm1):
                ice = floes[i].ice
                arr[i, :nvp3, :] = -ice.transmission_wtoi * water_phase(i + 1)
                arr[i, nvp3:, :] = -ice.reflection_w * water_phase(i + 1)
            indices = np.arange(1, nf)
            indptr = np.zeros(2 * nf + 1, dtype=int)
            indptr[0:-1:2] = np.arange(0, nf)
            indptr[1::2] = np.arange(0, nf)
            indptr[-1] = nfm1
            indices1 = np.arange(2, 2 * nf, 2)
            indptr1 = np.zeros(nfp1, dtype="int")
            indptr1[1:] = np.arange(0, nf)
            diag = ss.bsr_matrix(
                (
                    np.broadcast_to(np.eye(nvp1, hbs, dtype="int8"), (nfm1, nvp1, hbs)),
                    indices1,
                    indptr1,
                ),
                shape=(nvp1 * nf, bs * nf),
            ).tocsr()
            diag.eliminate_zeros()
            lhs += (
                ss.bsr_matrix(
                    (arr, indices, indptr), shape=(nf * bs, nf * nvp1)
                ).tocsr()
                @ diag
            )
            # 2nd block-diagonal, lower
            for i in range(nfm1):
                ice = floes[i + 1].ice
                arr[i, :nvp1, :] = -ice.reflection_w * water_phase(i + 1)
                arr[i, nvp1:, :] = -ice.transmission_wtoi * water_phase(i + 1)
            indptr[1:] = indptr[0:-1]
            indptr[0] = 0
            indptr1[0:-1] = indptr1[1:]
            indptr1[-1] = nfm1
            diag = ss.bsr_matrix(
                (
                    np.broadcast_to(
                        np.eye(nvp1, hbs, nvp3, dtype="int8"), (nfm1, nvp1, hbs)
                    ),
                    indices1 - 1,
                    indptr1,
                ),
                shape=(nvp1 * nf, bs * nf),
            ).tocsr()
            diag.eliminate_zeros()
            lhs += (
                ss.bsr_matrix(
                    (arr, indices - 1, indptr), shape=(nf * bs, nf * nvp1)
                ).tocsr()
                @ diag
            )

            # forcing; it is assumed that nothing comes from the right
            arr = np.empty((hbs, 1), dtype=complex)
            arr[:nvp1, 0] = floes[0].ice.reflection_w[:, 0]
            arr[nvp1:hbs, 0] = floes[0].ice.transmission_wtoi[:, 0]
            arr *= self.wave.potential_amplitude
            rhs = ss.csc_matrix(arr)

            # si le dernier floe est semi-infini, cas spécial :
            # le dernier edge est une transition eau vers glace,
            # un demi-bloc de moins est nécessaire.
            if floes[-1].length == np.inf:
                lhs = lhs[: nfm1 * bs + hbs, : nfm1 * bs + hbs]
                rhs.resize(((nfm1 * bs + hbs, 1)))
            else:
                rhs.resize(((nf * bs, 1)))

            # résolution du système
            # spsolve options from benchmark on laptop
            sol = ss.linalg.spsolve(lhs, rhs, "NATURAL", False)

            ww_neg = (sol[i * bs : i * bs + nvp1] for i in range(nf))
            iw_pos = (sol[i * bs + nvp1 : i * bs + hbs] for i in range(nf))
            if floes[-1].length == np.inf:
                # No right_edge, so it doesn't radiate anything
                l_iw_neg = [None]
                l_ww_pos = [None]
            else:
                # Contribution of the right edge of the rightmost floe
                l_iw_neg = [sol[nfm1 * bs + hbs : nfm1 * bs + hbs + nvp3]]
                l_ww_pos = [sol[nfm1 * bs + hbs + nvp3 : nf * bs]]
            iw_neg = (sol[i * bs + hbs : i * bs + hbs + nvp3] for i in range(nfm1))
            ww_pos = (sol[i * bs + hbs + nvp3 : (i + 1) * bs] for i in range(nfm1))

            return [
                FloeCoupled(floe, (iw_n, iw_p, ww_n, ww_p))
                for (floe, iw_n, iw_p, ww_n, ww_p) in zip(
                    floes,
                    itertools.chain(iw_neg, l_iw_neg),
                    iw_pos,
                    ww_neg,
                    itertools.chain(ww_pos, l_ww_pos),
                )
            ]

    def wave_energy(self) -> np.ndarray:
        """Return the wave energy (m**2) in the domain open-water regions.

        Wave energy is proportional to the squared amplitude of the wave.
        It is common in oceanography to express only this squared amplitude
        instead of the actual energy in J. The energy flux in the direction
        of progatation is obtained by multiplying by half the product of the
        fluid density and the acceleration of gravity, yielding a result in
        J mm**-2.
        This method consider each sub-domain whitout ice cover, adds the
        contributions of the left- and right propagating waves and returns an
        array containing the result. Only the amplitude corresponding to the
        actual propagating mode are considered.

        Returns
        -------
        1D array-like of float
            The resulting wave energies.

        """
        coef = (self.wave.angular_frequency / self.wave.gravity) ** 2
        n_floes = len(self.floes)
        if n_floes == 0:
            energ = np.empty(1)
        else:
            if self.floes[-1].length == np.inf:
                energ = np.empty(n_floes)
                energ[1:] = [
                    (
                        np.abs(fm.amplitude_ww_pos[0]) ** 2
                        + np.abs(fp.amplitude_ww_neg[0]) ** 2
                    )
                    for fm, fp in zip(self.floes[:-1], self.floes[1:])
                ]
            else:
                energ = np.empty(n_floes + 1)
                energ[1:-1] = [
                    (
                        np.abs(fm.amplitude_ww_pos[0]) ** 2
                        + np.abs(fp.amplitude_ww_neg[0]) ** 2
                    )
                    for fm, fp in zip(self.floes[:-2], self.floes[1:-1])
                ]
                energ[-1] = np.abs(self.floes[-1].amplitude_ww_pos[0]) ** 2
            energ[0] = np.abs(self.floes[0].amplitude_ww_neg[0]) ** 2
        energ[0] += np.abs(self.wave.potential_amplitude) ** 2

        return energ * coef

    def breakup(
        self, threshold: float = 5e-5, min_w: float = None, init_w: float = None
    ) -> int:
        """Trigger strain-based breakup for all the floes in the domain.

        This method looks up for the first three strain extrema undergone by
        each floe. The greatest among these three is assume to be the greatest
        for the floe (it is usually the 2nd; might be the 1st depending on the
        phase of the incoming wave; taking a 3rd one is a safety measure) and
        is compared to the given `threshold`. The floes break when this strain
        extrema exceed the `threshold`.

        Parameters
        ----------
        threshold : float
            A strain threshold in m m**-1.
        min_w: float
            Minimum width for random sampling when positionning floes.
        init_w: float
            Half-width of random sampling for the leftmost positionned floe.

        Returns
        -------
        int
            The number of new floes.

        """

        # Strain extrema happen roughly every half wavelength;
        # to be sure to capture at least 3, set clipping_th to 2
        # (hence 4 half wavelengths).
        def new_lengths(floe: Floe) -> list[float]:
            x_max = floe.strain_extrema(self.wave, n_coord=1, clipping_th=2).item()
            if np.abs(np.real(floe.strain(x_max, self.wave))) > threshold:
                return [x_max - floe.left_edge, floe.left_edge + floe.length - x_max]
            else:
                return [floe.length]

        # minimum interval width on which new floes positions are
        # sampled using a uniform distribution
        MIN_W = 0.01 if min_w is None else min_w
        # half-width of the interval, centered around the leftmost floe's left
        # edge, in which its new position will be picked
        INIT_H_WIDTH = self.ocean.wavelength if init_w is None else init_w

        n_floes_old = len(self.floes)
        lengths = [
            length
            for sublist in [new_lengths(f) for f in self.floes]
            for length in sublist
        ]
        n_floes_new = len(lengths)
        if n_floes_new == n_floes_old:
            return 0
        else:
            # for now, we know only one type of ice exists
            # so we don't waste time and space recording each floe's ice
            ip = self.floes[0].ice  # TEMP
            edges = np.empty(n_floes_new)
            domain_left = self.floes[0].left_edge - INIT_H_WIDTH
            domain_right = self.floes[0].left_edge + INIT_H_WIDTH

            for i, lb in enumerate(lengths):
                edges[i] = (
                    domain_left - domain_right
                ) * np.random.default_rng().random() + domain_right  # value sampled in (d_l, d_r]
                domain_left = edges[i] + lb
                # d_r[i] - x[i] := d_r[i] + L[i] - d_l[i+1]:
                # this condition enforces that the interval width never
                # reaches 0
                if domain_right - edges[i] > MIN_W:
                    domain_right += lb
                else:
                    domain_right = domain_left + MIN_W

            self.floes.clear()
            self.add_floes(
                [
                    Floe(left_edge=edge, length=length, ice=ip)
                    for edge, length in zip(edges, lengths)
                ]
            )
            return n_floes_new - n_floes_old


class FloeOverlapError(Exception):
    """Exception raised when adding a Floe overlapping with an existing one.

    Parameters
    ----------
    l_edges : tuple of float
        Left edges of the floes to be added.
    r_edges : tuple of float
        Right edges of the floes to be added.

    """

    def __init__(self, l_edges: tuple[float], r_edges: tuple[float]) -> None:
        """Initialise self."""
        super().__init__(
            f"The floe spawning from {l_edges[1]:.2f} to {r_edges[1]:.2f} "
            f"overlaps with the floe spawning from {l_edges[0]:.2f}"
            f"to {r_edges[0]:.2f}."
        )
