# Scattered wave inducing sea ice breakup

1D model of plane wave scattering by floating elastic plates, developed to study wave-induced sea ice breakup.

This is part of a PhD projet conducted at the University of Otago.

Applications are detailed in an [article](https://tc.copernicus.org/articles/16/4447/2022/tc-16-4447-2022.html) published in The Cryosphere.
