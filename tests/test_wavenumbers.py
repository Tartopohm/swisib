#!/usr/bin/env python3

"""Test that computed wave numbers check out dispertion relations."""

import unittest
import numpy as np
from wisib.scattering2d import Floe, Ocean, Wave, Domain, Ice


def dr_ocean(d: Domain, k: np.ndarray) -> np.ndarray:
    """Dispersion relation for free surface water waves."""
    return k*np.tanh(k*d.ocean.depth) - d.wave._c_alpha


def dr_ice(d: Domain, f: Floe, k: np.ndarray) -> np.ndarray:
    """Dispersion relation for ice-covered water waves."""
    D = f.ice.flex_rigidity
    rw = d.ocean.density
    g = d.wave.gravity
    om = d.wave.angular_frequency
    om2 = d.wave.angular_frequency2
    H = d.ocean.depth
    dr = f.ice.draft
    vis = f.ice.viscosity
    return ((D*k**4/(g*rw) + 1 - dr*om2/g - 1j*om*vis/(g*rw))
            * k*np.tanh(k*(H-dr)) - om2/g)


class TestWavenumbers(unittest.TestCase):
    """Generic test class."""

    def test_ocean(self) -> None:
        """Test the DR for free surface waves."""
        for dpth in np.logspace(0, 4, 13):
            d = Domain(Wave(), Ocean(depth=dpth), nk=50)
            res = dr_ocean(d, d.ocean.wave_numbers)
            self.assertAlmostEqual(np.abs(res).max(), 0)

    def test_ice(self) -> None:
        """Test the DR for ice-covered surface waves."""
        for vsct in [0, 1, 5, 10, 20, 30, 50, 100]:
            d = Domain(Wave(), Ocean(), nk=50)
            f = Floe(ice=Ice(viscosity=vsct))
            d.add_floes([f])
            f = d.floes[0]

            res = dr_ice(d, f, f.ice.wave_numbers)
            mx, amx = np.abs(res).max(), np.abs(res).argmax()
            self.assertAlmostEqual(mx, 0, 7,
                                   'vsct: {:.2f}, '.format(vsct)
                                   + 'index: {:d}'.format(amx))

        for dpth in np.logspace(np.log10(48), 4, 10):
            d = Domain(Wave(), Ocean(depth=dpth), nk=50)
            f = Floe()
            d.add_floes([f])
            f = d.floes[0]

            res = dr_ice(d, f, f.ice.wave_numbers)
            mx, amx = np.abs(res).max(), np.abs(res).argmax()
            self.assertAlmostEqual(mx, 0, 6,
                                   'depth: {:.2f}, '.format(dpth)
                                   + 'index: {:d}'.format(amx))

        for tkns in np.logspace(np.log10(.1), np.log10(2.5), 9):
            d = Domain(Wave(), Ocean(), nk=50)
            f = Floe(ice=Ice(thickness=tkns))
            d.add_floes([f])
            f = d.floes[0]

            res = dr_ice(d, f, f.ice.wave_numbers)
            self.assertAlmostEqual(np.abs(res).max(), 0, 6,
                                   'thickness; {:.2f}'.format(tkns))

        for perd in np.logspace(np.log10(4), np.log10(20), 9):
            d = Domain(Wave(period=perd), Ocean(), nk=50)
            f = Floe()
            d.add_floes([f])
            f = d.floes[0]

            res = dr_ice(d, f, f.ice.wave_numbers)
            self.assertAlmostEqual(np.abs(res).max(), 0)


if __name__ == '__main__':
    unittest.main()
