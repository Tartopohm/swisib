#!/usr/bin/env python3

from numpy import cosh, nan, ndarray, exp, trapz, abs as nabs
from src.scattering2d.scattering2d import Floe, Ocean, Wave, Domain


def vmode_w(domain: Domain, z: ndarray, k: complex) -> ndarray:
    return cosh(k*(domain.ocean.depth + z))/cosh(k*domain.ocean.depth)


def vmode_i(domain: Domain, z: ndarray, k: complex, floe) -> ndarray:
    vm = cosh(k*(domain.ocean.depth + z))/cosh(k*floe.dud)
    vm[z > - floe.draft] = nan
    return vm


def potential_w(domain: Domain, floe1: Floe, floe2: Floe = None,
                left: bool = True, d: bool = False) -> ndarray:
    amp = domain.wave.potential_amplitude
    # floe2 à droite de floe1
    if floe2 is None:  # un seul floe: soit le premier, soit le dernier
        # le premier: j-1 = -inf, forçage par amplitude réelle de la gauche
        if left:
            wp_js = [amp] + domain.nv*[0]
            wn_js = floe1.amplitude_ww_neg
        else:  # le dernier: j+1 = +inf, forçage nul de la droite
            wp_js = floe1.amplitude_ww_pos
            wn_js = (domain.nv+1)*[0]
    else:
        nvp1 = floe1.transmission.shape[0]
        # de la gauche vient l'onde, déphasée, due à floe1
        # (ON CALCULE LA CONTRIB DE L'EDGE DE floe2)
        if left:
            wp_js = (
                floe1.amplitude_ww_pos
                * exp(1j*domain.ocean.wave_numbers[:nvp1]
                      * (floe2.left_edge - (floe1.left_edge+floe1.length)))
            )
            wn_js = floe2.amplitude_ww_neg
        # de la droite vient l'onde, déphasée, due à floe2
        # (ON CALCULE LA CONTRIB DE L'EDGE DE floe1)
        else:
            wp_js = floe1.amplitude_ww_pos
            wn_js = (
                floe2.amplitude_ww_neg
                * exp(1j*domain.ocean.wave_numbers[:nvp1]
                      * (floe2.left_edge - (floe1.left_edge+floe1.length)))
            )

    if not d:
        return sum(
            [(wp_j + wn_j)*vmode_w(k_j)
             for wp_j, wn_j, k_j
             in zip(wp_js, wn_js, domain.ocean.wave_numbers[:domain.nv+1])]
        )
    else:
        return 1j*sum(
            [k_j*(wp_j - wn_j)*vmode_w(k_j)
             for wp_j, wn_j, k_j
             in zip(wp_js, wn_js, domain.ocean.wave_numbers[:domain.nv+1])]
        )


def potential_i(domain: Domain, floe: Floe, z: ndarray,
                left: bool = True, d: bool = False) -> ndarray:
    if left:
        wp_js = floe.amplitude_iw_pos
        wn_js = floe.amplitude_iw_neg * exp(1j*floe.phase)
    else:
        wp_js = floe.amplitude_iw_pos * exp(1j*floe.phase)
        wn_js = floe.amplitude_iw_neg

    if not d:
        pot = sum(
            [(wp_j + wn_j)*vmode_i(k_j, floe)
             for wp_j, wn_j, k_j
             in zip(wp_js, wn_js, floe.wave_numbers[:domain.nv+3])]
        )
    else:
        pot = 1j*sum(
            [k_j*(wp_j - wn_j)*vmode_i(k_j, floe)
             for wp_j, wn_j, k_j
             in zip(wp_js, wn_js, floe.wave_numbers[:domain.nv+3])]
        )

    pot[z > -floe.draft] = nan + 1j*nan
    return pot


def mse(arr1: ndarray, arr2: ndarray, z: ndarray, msk: ndarray) -> float:
    return (trapz((nabs(arr1 - arr2)**2)[msk], z[msk])
            / (z[msk].max() - z[msk].min()))
