#!/usr/bin/env python3

"""Basic tests for the API of the model."""

import unittest
import numpy as np
from wisib.scattering2d import Ocean, Floe, Wave, Domain, Ice
from wisib.scattering2d import FloeOverlapError


class TestDomain(unittest.TestCase):
    """Generic test class."""

    def test_add_ocean(self):
        """Test the correct initialisation of the domain."""
        self.assertRaises(TypeError, Domain)

        w = Wave()
        self.assertRaises(TypeError, Domain, w)

        o = Ocean()
        d = Domain(w, o)

        self.assertIsInstance(d.ocean.inner_products, np.ndarray)
        self.assertTrue(d.ocean.inner_products.shape == (d.nk + 1,))
        self.assertIsInstance(d.ocean.wave_numbers, np.ndarray)
        self.assertTrue(d.ocean.wave_numbers.shape == (d.nk + 1,))

    def test_add_first_floe(self):
        """Test the correct initialisation of a single floe."""
        w = Wave()
        o = Ocean()
        d = Domain(w, o)
        self.assertListEqual(d.floes, [], "Default value")

        f = Floe()
        # self.assertIsNone(f.ice.inner_products, "Default state")
        # self.assertIsNone(f.ice.wave_numbers, "Default state")
        # self.assertRaises(AssertionError, d.add_floes, [f])

        # d.add_wave(w)
        # self.assertRaises(AssertionError, d.add_floes, [f])

        # d.set_ocean()
        d.add_floes((f,))
        self.assertTrue(len(d.floes) == 1)
        self.assertIsInstance(d.floes[0].ice.inner_products,
                              np.ndarray)
        self.assertTrue(
            d.floes[0].ice.inner_products.shape == (d.nk + 3,))
        self.assertIsInstance(d.floes[0].ice.wave_numbers,
                              np.ndarray)
        self.assertTrue(
            d.floes[0].ice.wave_numbers.shape == (d.nk + 3,))

    def test_add_second_floe(self):
        """Test the correct initialisation of a second floe."""
        w = Wave()
        o = Ocean()
        d = Domain(w, o)
        f1 = Floe(left_edge=100, length=100)
        d.add_floes((f1,))

        f2 = Floe(left_edge=80, length=20)
        msg = 'Should fail as left edge of f2 equals left edge of f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=200, length=20)
        msg = 'Should fail as left edge of f2 equals right edge of f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=80, length=40)
        msg = 'Should fail as f2 ends within f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=180, length=40)
        msg = 'Should fail as f2 starts within f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=80, length=120)
        msg = 'Should fail as f2 completely overlaps f1 from the left'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=100, length=120)
        msg = 'Should fail as f2 completely overlaps f1 to the right'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=80, length=140)
        msg = 'Should fail as f2 completely overlaps f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=100, length=100)
        msg = 'Should fail as f2 "equals" f1'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f2,))

        f2 = Floe(left_edge=300)
        d.add_floes((f2,))
        self.assertTrue(len(d.floes) == 2)
        self.assertIs(f1.ice, f2.ice)
        self.assertIs(d.floes[0].ice, d.floes[1].ice)
        # ip = f1.ice
        # self.assertIsInstance(ip.inner_products, np.ndarray)
        # self.assertTrue(ip.inner_products.shape == (d.nk + 3,))
        # self.assertIsInstance(ip.wave_numbers, np.ndarray)
        # self.assertTrue(ip.wave_numbers.shape == (d.nk + 3,))

    def test_add_nth_floe(self):
        """Test the correct initialisation of successively added floes."""
        w = Wave()
        o = Ocean()
        d = Domain(w, o)
        f1 = Floe(left_edge=100, length=100)  # 100 to 200
        f2 = Floe(left_edge=300, length=100)  # 300 to 400
        # d.add_wave(w)
        # d.set_ocean()
        d.add_floes((f1, f2))
        # d.add_floe(f2)

        f3 = Floe(left_edge=180, length=40)
        msg = 'Should fail as f3 starts before f1 ends'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=200, length=40)
        msg = 'Should fail as f3\'s left edge equals f1\'s right edge'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=280, length=40)
        msg = 'Should fail as f3 ends after f2 starts'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=260, length=40)
        msg = 'Should fail as f3\'s right edge equals f2\'s left edge'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=180, length=140)
        msg = 'Should fail as f3 overlaps both f1 and f2'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=200, length=300)
        msg = 'Should  fail as f3\'s left is f1\'s right,\
            f3\'s right is f2\'s left'
        with self.assertRaises(FloeOverlapError, msg=msg):
            d.add_floes((f3,))

        f3 = Floe(left_edge=240, length=20)
        d.add_floes((f3,))
        self.assertTrue(len(d.floes) == 3)
        self.assertIs(d.floes[0].ice, d.floes[2].ice)

        f4 = Floe(left_edge=420, length=20)
        d.add_floes((f4,))
        self.assertTrue(len(d.floes) == 4)
        self.assertIs(d.floes[0].ice, d.floes[3].ice)

        f5 = Floe(left_edge=20, length=20,
                  ice=Ice(density=900))
        d.add_floes((f5,))
        self.assertTrue(len(d.floes) == 5)
        # self.assertIsNot(f1.ice, f5.ice)
        self.assertIsNot(d.floes[0].ice, d.floes[4].ice)
        # self.assertIsInstance(f5.ice.inner_products, np.ndarray)
        # self.assertTrue(f5.ice.inner_products.shape == (d.nk + 3,))
        # self.assertIsInstance(f5.ice.wave_numbers, np.ndarray)
        # self.assertTrue(f5.ice.wave_numbers.shape == (d.nk + 3,))


if __name__ == '__main__':
    unittest.main()
