#!/usr/bin/env python3

"""Tests for analytical expression of inner products."""

import unittest
import numpy as np
from wisib.scattering2d import Floe, Ocean, Wave, Domain


class TestInnerProducts(unittest.TestCase):
    """Basic test class."""

    def test_ocean_ip(self) -> None:
        """Test if analytic IP correspond to numerical integration (ocean)."""
        d = Domain(Wave(), Ocean(), nk=50)
        nz = 4001
        z = np.zeros(nz)
        z[1:] = np.logspace(np.log10(1e-5), np.log10(d.ocean.depth), nz-1)
        z = -z[::-1]

        for k, ip in zip(d.ocean.wave_numbers, d.ocean.inner_products):
            integ = np.trapz(
                (np.cosh(k*(d.ocean.depth + z))/np.cosh(k*d.ocean.depth))**2, z
            )
            self.assertEqual(np.imag(integ), 0)
            self.assertAlmostEqual(1, ip/np.real(integ), 5)

    def test_floe_ip(self) -> None:
        """Test if analytic IP correspond to numerical integration (ice)."""
        d = Domain(Wave(), Ocean(), nk=50)
        d.add_floes((Floe(),))
        f = d.floes[0]
        nz = 4001
        z = np.logspace(np.log10(f.ice.draft), np.log10(d.ocean.depth), nz)
        z = -z[::-1]

        for k, ip in zip(f.ice.wave_numbers, f.ice.inner_products):
            integ = (np.trapz((np.cosh(k*(d.ocean.depth + z))
                               / np.cosh(k*f.ice.dud))**2,
                              z)
                     + 2*f.ice._c_length**5*k**4*np.tanh(k*f.ice.dud)**2)

            self.assertAlmostEqual(1, ip/integ, 5)
