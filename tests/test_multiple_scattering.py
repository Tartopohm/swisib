#!/usr/bin/env python3

"""Test energy conservation for scattering by an array of floes."""

import unittest
import numpy as np
from numpy.random import rand as nrand
from wisib.scattering2d import Floe, Ocean, Wave, Domain

NK, NU, NV = 100, 20, 2


class TestMultipleScattering(unittest.TestCase):
    """Generic test class."""

    def test_alt_scattering(self) -> None:
        """Test case where last floe is finite."""
        d = Domain(Wave(), Ocean(), nk=NK, nu=NU, nv=NV)
        nb_floes = 1000
        domain_length = nb_floes*2*np.pi/np.real(d.ocean.wave_numbers[0])

        edge_locations = np.sort(domain_length*np.random.rand(nb_floes*2))
        left_edges = edge_locations[::2]
        lengths = edge_locations[1::2] - left_edges
        lf = [Floe(left_edge=le, length=lgth)
              for le, lgth in zip(left_edges, lengths)]

        d.add_floes(lf)
        lf = d.floes

        rho = lf[0].amplitude_ww_neg[0]/d.wave.potential_amplitude
        tau = lf[-1].amplitude_ww_pos[0]/d.wave.potential_amplitude

        self.assertAlmostEqual(np.abs(rho)**2 + np.abs(tau)**2, 1)

        for i in range(nb_floes):
            if i == 0:
                wwp = d.wave.potential_amplitude
            else:
                wwp = lf[i-1].amplitude_ww_pos[0]
            if i == nb_floes - 1:
                wwn = 0
            else:
                wwn = lf[i+1].amplitude_ww_neg[0]
            self.assertAlmostEqual(np.abs(wwp)**2 + np.abs(wwn)**2,
                                   (np.abs(lf[i].amplitude_ww_neg[0])**2
                                    + np.abs(lf[i].amplitude_ww_pos[0])**2),
                                   msg='floe {:d}'.format(i), places=4)

    def test_alt_scattering_inf(self) -> None:
        """Test case where last floe is infinite."""
        d = Domain(Wave(), Ocean(), nk=NK, nu=NU, nv=NV)
        # d.add_wave(Wave())
        # d.set_ocean(depth=1000)
        nb_floes = 1000
        domain_length = nb_floes*2*3.1415/np.real(d.ocean.wave_numbers[0])

        edge_locations = np.sort(domain_length*nrand(nb_floes*2))
        left_edges = edge_locations[::2]
        lengths = edge_locations[1::2] - left_edges
        lengths[-1] = np.inf
        lf = [Floe(left_edge=le, length=lgth)
              for le, lgth in zip(left_edges, lengths)]

        d.add_floes(lf)
        lf = d.floes

        rho = lf[0].amplitude_ww_neg[0]/d.wave.potential_amplitude
        tau = lf[-1].amplitude_iw_pos[2]/d.wave.potential_amplitude
        kg0 = lf[-1].ice.wave_numbers[2]
        k0h = d.ocean.wave_numbers[0]*d.ocean.depth
        kg0h = kg0*lf[-1].ice.dud
        kg0, k0h, kg0h = np.real(kg0), np.real(k0h), np.real(kg0h)
        th0, thg0 = np.tanh(k0h), np.tanh(kg0h)
        coef_tau = kg0h*(
            1 + thg0/kg0h
            - (1 - 4*lf[-1].ice.flex_rigidity
               * kg0**4
               / (d.ocean.density*d.wave.angular_frequency2
                  * lf[-1].ice.dud))*thg0**2
        )/(k0h*(1 + th0/k0h - th0**2))
        self.assertAlmostEqual(np.abs(rho)**2 + coef_tau*np.abs(tau)**2, 1)

        for i in range(nb_floes-1):
            if i == 0:
                wwp = d.wave.potential_amplitude
            else:
                wwp = lf[i-1].amplitude_ww_pos[0]
            if i == nb_floes - 1:
                wwn = 0
            else:
                wwn = lf[i+1].amplitude_ww_neg[0]
            self.assertAlmostEqual(np.abs(wwp)**2 + np.abs(wwn)**2,
                                   (np.abs(lf[i].amplitude_ww_neg[0])**2
                                    + np.abs(lf[i].amplitude_ww_pos[0])**2),
                                   msg='floe {:d}'.format(i), places=4)
