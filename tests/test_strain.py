#!/usr/bin/env python3

"""Strain-related tests."""

import unittest
import numpy as np
from wisib.scattering2d import Ocean, Floe, Wave, Domain


class TestDomain(unittest.TestCase):
    """Generic test class."""

    def test_strain_bounds(self):
        """Check the strain-free boundary conditions."""
        d = Domain(Wave(), Ocean())
        # w = Wave()
        # d.add_wave(w)
        # d.set_ocean()
        f = Floe()
        d.add_floes((f,))
        # d.alt_scattering()
        f = d.floes[0]
        # print(f.amplitude_iw_neg)
        # print(f.amplitude_iw_pos)
        # print(f.amplitude_ww_neg)
        # print(f.amplitude_ww_pos)

        x = np.linspace(f.left_edge, f.left_edge+f.length)
        s = f.strain(x, d.wave)

        self.assertAlmostEqual(np.abs(s[0]), 0)
        self.assertAlmostEqual(np.abs(s[-1]), 0)


if __name__ == '__main__':
    unittest.main()
