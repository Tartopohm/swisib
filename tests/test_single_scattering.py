#!/usr/bin/env python3

"""Tests for energy conservation for elastic ice."""

import unittest
from numpy import allclose, tanh
from numpy import all as nall
from numpy import real, imag
from numpy import abs as nabs
from wisib.scattering2d import Floe, Ocean, Wave, Domain


class TestInnerProducts(unittest.TestCase):
    """Generic test class."""

    def test_semi_inf_energy(self) -> None:
        """Test energy conservation for scattering by a semi-infinite floe."""
        w = Wave()
        o = Ocean()
        d = Domain(w, o)
        d.add_floes([Floe()])
        f = d.floes[0]

        rho = f.ice.reflection_w[0, 0]
        tau = f.ice.transmission_wtoi[2, 0]
        rho2 = nabs(rho)**2
        tau2 = nabs(tau)**2

        kg0 = f.ice.wave_numbers[2]
        assert allclose(0, imag(kg0)) and nall(real(kg0) > 0)
        k0h = d.ocean.wave_numbers[0]*d.ocean.depth
        kg0h = kg0*f.ice.dud
        assert allclose(0, imag(k0h)) and nall(real(k0h) > 0)
        kg0, k0h, kg0h = real(kg0), real(k0h), real(kg0h)
        th0, thg0 = tanh(k0h), tanh(kg0h)
        coef_tau = kg0h*(1 + thg0/kg0h
                         - (1
                            - 4*f.ice.flex_rigidity
                            * kg0**4
                            / (d.ocean.density*d.wave.angular_frequency2
                               * f.ice.dud))*thg0**2
                         )/(k0h*(1 + th0/k0h - th0**2))
        self.assertAlmostEqual(rho2 + coef_tau*tau2, 1)

    def test_finite_energy(self) -> None:
        """Test energy conservation for scattering by a finite floe."""
        d = Domain(Wave(), Ocean())
        # d.add_wave(Wave())
        # d.set_ocean()
        d.add_floes([Floe()])
        f = d.floes[0]

        # d.alt_scattering()

        rho = f.amplitude_ww_neg[0]/d.wave.potential_amplitude
        tau = f.amplitude_ww_pos[0]/d.wave.potential_amplitude
        rho2 = nabs(rho)**2
        tau2 = nabs(tau)**2

        self.assertAlmostEqual(rho2 + tau2, 1)
